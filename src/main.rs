#[macro_use]
extern crate log;
extern crate simple_logger;
#[macro_use]
extern crate anyhow;

mod cdc;

use anyhow::Result;
use std::fs::File;
use std::io::prelude::*;

fn main() -> Result<()> {
    simple_logger::init_with_level(log::Level::Debug).unwrap();

    let args: Vec<_> = ::std::env::args_os().collect();

    if args.len() < 2 {
        error!("Usage: pcx-cd <mc68hc05-rom-dump> [scan-target]");
        bail!("Missing ROM");
    }

    let mut f = File::open(&args[1])?;
    let mut rom = [0; cdc::MC68HC05_ROM_DUMP_SIZE];

    f.read_exact(&mut rom)?;

    let mut cdc = Box::new(cdc::Cdc::new(&rom));

    if let Some(scan) = args.get(2) {
        let scan = match scan.clone().into_string() {
            Ok(s) => s,
            Err(_) => bail!("Invalid scan target"),
        };

        let target: Result<Vec<u16>, _> = scan.split('.').map(parse_u16).collect();
        let target = target?;

        let target = match target.as_slice() {
            &[a] => cdc::ScanMode::Byte(a),
            &[a, b] => cdc::ScanMode::Bit(a, b as u8),
            _ => bail!("Invalid scan target '{}', expected 'byte[.bit]'", scan),
        };

        cdc.scan(target);
        return Ok(());
    }

    cdc.set_debug(false);

    // Start by pretending that the shell is open
    cdc.set_shell_open(true);

    cdc.run_seconds(1);

    info!("Send command 0x01 (Getstat)");
    // Select index 0
    cdc.host_write(0, 0x00);
    // Send command 0x01: Getstat
    cdc.host_write(1, 0x01);

    cdc.run_seconds(1);

    info!("Send command 0x17 (Invalid)");
    // Select index 0
    cdc.host_write(0, 0x00);
    // Send command 0x17
    cdc.host_write(1, 0x17);

    cdc.run_seconds(1);

    // Close the shell
    info!("Closing shell...");
    cdc.set_shell_open(false);

    cdc.run_seconds(20);

    Ok(())
}

pub fn parse_u16(s: &str) -> Result<u16> {
    let n = if s.starts_with("0x") {
        let (_, n) = s.split_at(2);

        u16::from_str_radix(n, 16)?
    } else {
        u16::from_str_radix(&s, 10)?
    };

    Ok(n)
}
